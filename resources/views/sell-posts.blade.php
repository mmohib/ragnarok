@extends('layouts.master')

@section('title', 'Book')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="sell-exchange-books">
        <div class="container">
            <div class="row">

                <div class="section-heading text-center text-capitalize">
                    <h1>All Sell Post</h1>
                </div>

                <div class="row">

                    @foreach ($sellPosts as $sellPost)
                        <div class="col-xs-6 col-sm-4 col-md-3">

                            <div class="book-wrapper">
                                <a href="{{ route('book', ['book-id' => $sellPost->book_id, 'user-id' => $sellPost->user_id ])}}">
                                    <div class="book-image">
                                        @if (isset($sellPost->book->allUser->where('pivot.user_id', $sellPost->user->id)[0]->pivot->image))
                                            <img class="img-responsive" src="/images/books/{{ $sellPost->book->allUser->where('pivot.user_id', $sellPost->user->id)[0]->pivot->image }}" alt="book cover" />
                                        @else
                                            <img class="img-responsive" src="/images/default-cover.jpg" alt="book cover" />
                                        @endif
                                    </div>
                                    <div class="book-meta text-center text-capitalize">
                                        <p>
                                            <em>৳</em>{{ $sellPost->price }}
                                        </p>
                                        <div class="star-rating">
                                            @if($sellPost->book->ratings->avg('value'))
                                                @for ($i = 0; $i < $sellPost->book->ratings->avg('value'); $i++)
                                                    <i class="ion-ios-star"></i>
                                                @endfor
                                            @else
                                                <p>No rating yet</p>
                                            @endif
                                        </div>
                                        <p>{{ $sellPost->book->title }}</p>
                                    </div>

                                </a>
                                <div class="book-buttons">
                                    <a href="{{ route('book', ['book-id' => $sellPost->book_id, 'user-id' => $sellPost->user_id ])}}">Buy Now</a>
                                </div>
                            </div>

                        </div>
                    @endforeach


                    
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        {{ $sellPosts->links() }}
                    </div>
                </div>

            </div>
        </div>
    </section>

    
@endsection

@section('javascript')
	@parent
@endsection