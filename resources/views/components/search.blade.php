<div class="site-search">
    <form action="{{ route('search') }}" method="get">
        <div class="form-group">
            <input name="q" id="search" class="form-control" type="text" placeholder="Search Books...">
            <button type="submit">
                <img width="40" height="30" fill="#fff" src="svg/search.svg" alt="">
            </button>
        </div>
    </form>
</div>