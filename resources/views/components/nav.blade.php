<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/images/logo.svg" alt="logo">
                <span>BookShelf</span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
                
                @guest
                    <a href="{{ route('register') }}" class="btn btn-default navbar-btn">Register</a>
                    <a href="{{ route('login') }}" class="btn btn-primary navbar-btn">Sign in</a>
                @else
                    <a href="{{ url('/dashboard') }}" class="btn btn-default navbar-btn">Dashboard</a>
                    <a href="{{ route('logout') }}"
                        class="btn btn-back navbar-btn"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        Sign out
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>              
                @endguest
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>