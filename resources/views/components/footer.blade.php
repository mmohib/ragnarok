<footer>
    <div class="row">
        <div class="col-xs-offset-1 col-xs-5">
            <p>The application is under the MIT license</p>
        </div>
        <div class="col-xs-5">
            <p class="text-right">Designed & Developed by Mohib & Maruf</p>
        </div>
    </div>
</footer>