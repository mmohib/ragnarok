<section id="header">
        <div class="container">
            <h1 class="text-center">Sell. Exchange. Find Review. <em>Make Library</em></h1>
            <div class="row">
                @include('components.search')
            </div>
        </div>
    </section>