@extends('layouts.master')

@section('title', 'Book')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="sell-exchange-books">
        <div class="container">
            <div class="row">

                {{--  Header Section  --}}
                @include('components.search')

                <div class="section-heading text-center text-capitalize">
                    <h1>Search Results</h1>
                </div>

                <div class="row">

                    @foreach($books as $book)

                        @if($book->sellPosts)
                            @foreach ($book->sellPosts as $sellPost)
                                <div class="col-xs-6 col-sm-4 col-md-3">

                                    <div class="book-wrapper">
                                        <a href="{{ route('book', ['book-id' => $sellPost->book_id, 'user-id' => $sellPost->user_id ])}}">
                                            <div class="book-image">
                                                @if (isset($sellPost->book->allUser->where('pivot.user_id', $sellPost->user->id)[0]->pivot->image))
                                                    <img class="img-responsive" src="/images/books/{{ $sellPost->book->allUser->where('pivot.user_id', $sellPost->user->id)[0]->pivot->image }}" alt="book cover" />
                                                @else
                                                    <img class="img-responsive" src="/images/default-cover.jpg" alt="book cover" />
                                                @endif
                                                <div class="image-badge">
                                                    Sell
                                                </div>
                                            </div>
                                            <div class="book-meta text-center text-capitalize">
                                                <p>
                                                    <em>৳</em>{{ $sellPost->price }}
                                                </p>
                                                <div class="star-rating">
                                                    @if($sellPost->book->ratings->avg('value'))
                                                        @for ($i = 0; $i < $sellPost->book->ratings->avg('value'); $i++)
                                                            <i class="ion-ios-star"></i>
                                                        @endfor
                                                    @else
                                                        <p>No rating yet</p>
                                                    @endif
                                                </div>
                                                <p>{{ $sellPost->book->title }}</p>
                                            </div>

                                        </a>
                                        <div class="book-buttons">
                                            <a href="{{ route('book', ['book-id' => $sellPost->book_id, 'user-id' => $sellPost->user_id ])}}">Buy Now</a>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        @endif

                        @if($book->exchangePosts)
                            @foreach ($book->exchangePosts as $exchangePost)
                        
                                <div class="col-xs-6 col-sm-4 col-md-3">
                                    <div class="book-wrapper">
                                        <a href="{{ route('book', ['book-id' => $exchangePost->book_id, 'user-id' => $exchangePost->user_id ])}}">
                                            <div class="book-image">
                                                @if (isset($exchangePost->book->allUser->where('pivot.user_id', $exchangePost->user->id)[0]->pivot->image))
                                                    <img class="img-responsive" src="/images/books/{{ $exchangePost->book->allUser->where('pivot.user_id', $exchangePost->user->id)[0]->pivot->image }}" alt="book cover" />
                                                @else
                                                    <img class="img-responsive" src="/images/default-cover.jpg" alt="book cover" />
                                                @endif
                                                <div class="image-badge">
                                                    Exchange
                                                </div>
                                            </div>
                                            <div class="book-meta text-center text-capitalize">
                                                <p style="visibility: hidden">
                                                    <em>Any</em>
                                                </p>
                                                <div class="star-rating">
                                                    @if($exchangePost->book->ratings->avg('value'))
                                                        @for ($i = 0; $i < $exchangePost->book->ratings->avg('value'); $i++)
                                                            <i class="ion-ios-star"></i>
                                                        @endfor
                                                    @else
                                                        <p>No rating yet</p>
                                                    @endif
                                                </div>
                                                <p>{{ $exchangePost->book->title }}</p>
                                            </div>

                                        </a>
                                        <div class="book-buttons exchange">
                                            <a href="{{ route('book', ['book-id' => $exchangePost->book_id, 'user-id' => $exchangePost->user_id ])}}">Exchange Now</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    
                    @endforeach
                    
                </div>

            </div>
        </div>
    </section>

    
@endsection

@section('javascript')
	@parent
@endsection