@extends('layouts.master')

@section('title', 'Book')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="sell-exchange-books">
        <div class="container">
            <div class="row">

                <div class="section-heading text-center text-capitalize">
                    <h1>All Exchange Post</h1>
                </div>

                <div class="row">
                    @foreach ($exchangePosts as $exchangePost)
                
                        <div class="col-xs-6 col-sm-4 col-md-3">

                            <div class="book-wrapper">
                                <a href="{{ route('book', ['book-id' => $exchangePost->book_id, 'user-id' => $exchangePost->user_id ])}}">
                                    <div class="book-image">
                                        @if (isset($exchangePost->book->allUser->where('pivot.user_id', $exchangePost->user->id)[0]->pivot->image))
                                            <img class="img-responsive" src="/images/books/{{ $exchangePost->book->allUser->where('pivot.user_id', $exchangePost->user->id)[0]->pivot->image }}" alt="book cover" />
                                        @else
                                            <img class="img-responsive" src="/images/default-cover.jpg" alt="book cover" />
                                        @endif
                                    </div>
                                    <div class="book-meta text-center text-capitalize">
                                        
                                        <div class="star-rating">
                                            @if($exchangePost->book->ratings->avg('value'))
                                                @for ($i = 0; $i < $exchangePost->book->ratings->avg('value'); $i++)
                                                    <i class="ion-ios-star"></i>
                                                @endfor
                                            @else
                                                <p>No rating yet</p>
                                            @endif
                                        </div>
                                        <p>{{ $exchangePost->book->title }}</p>
                                    </div>

                                </a>
                                <div class="book-buttons exchange">
                                    <a href="{{ route('book', ['book-id' => $exchangePost->book_id, 'user-id' => $exchangePost->user_id ])}}">Exchange Now</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        {{ $exchangePosts->links() }}
                    </div>
                </div>

            </div>
        </div>
    </section>

    
@endsection

@section('javascript')
	@parent
@endsection