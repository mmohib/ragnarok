@extends('dashboard.layouts.master')

@section('title', 'Welcome')

@section('stylesheet')
	@parent
@endsection

@section('content')
<div class="container">
        <div class="row">
            <h1 class="text-center">{{ $borrower->borrower_name }}</h1>
            <h4 class="text-center">{{ $borrower->email }}</h4>
            <h4 class="text-center">{{ $borrower->phone_number }}</h4>
            <div class="text-center">
                <a class="btn btn-default" href="{{ route('edit-borrower', ['id' => $borrower->id ] ) }}">Edit</a>
            </div>
            <br>
        </div>

        <div class="table-responsive report-table-desktop">
            <table class="table table-bordered">
                <caption><h3>All Books</h3></caption>
                <thead>
                    <th>Title</th>
                    <th>Lend Date</th>
                    <th>Expected Return Date</th>
                    <th>Return Date</th>
                    <th>Status</th>
                </thead>
        
                @foreach ($books as $book)
                    <tr>
                        
                        <td><a href="{{ route('dashboard-book', ['id' => $book->id])}}">{{ $book->title }}</a></td>
                        <td>{{ $book->pivot->lend_date }}</td>
                        <td>{{ $book->pivot->return_date }}</td>

                        @if ($book->pivot->orginal_return_date)
                            <td>{{ $book->pivot->orginal_return_date }}</td>
                        @else
                            <td>Not Returned Yet</td>
                        @endif

                        @if (!$book->pivot->orginal_return_date)
                            <td>
                                Lent To Borrower
                            </td>
                        @else
                            <td>
                                In the Shelf
                            </td>
                        @endif
                        
                    </tr>
                @endforeach
        
            </table>

        </div>
    </div>
@endsection

@section('javascript')
	@parent
@endsection