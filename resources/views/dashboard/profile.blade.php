@extends('dashboard.layouts.master')

@section('title', 'Profile')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="dashboard-book">
        <div class="container">

            <div class="row section-heading">
                <h1 class="text-center text-capitalize">{{ $user->name }}</h1>
            </div>

            <br><br>

            <div class="row">
                
                <div class="col-md-offset-1 col-sm-12 col-md-2">
                    <div id="book-fixed-section">
                        <div class="book-wrapper">
                            <div class="book-image">
                                @if ($user->image)
                                    <img class="img-responsive center-block" src="/images/profile/{{ $user->image }}" alt="book cover" />
                                @else
                                    <img class="img-responsive center-block" src="/images/dashboard/user.png" alt="book cover" />
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-4 col-md-offset-1">
                    <div class="row">
                        <table class="table">
                            <tr>
                                <td>Email:</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td>Mobile:</td>
                                <td>{{ $user->mobile }}</td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>{{ $user->address }}</td>
                            </tr>
                        </table>
                        <a class="btn btn-default" href="{{ route('edit-profile', ['id' => $user->id])}}" role="button">Edit Information</a>
                    </div>
                </div>
            </div>

            <br><br>
        </div>
    </section>

@endsection

@section('javascript')
	@parent
@endsection