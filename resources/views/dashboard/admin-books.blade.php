@extends('dashboard.layouts.master')

@section('title', 'All Books')

@section('stylesheet')
	@parent
@endsection

@section('content')


    <section class="report-main-content-desktop hidden-xs">
        <div class="container">

            <div class="table-responsive report-table-desktop">
                    <table class="table table-bordered">
                        <caption><h3>All Books</h3></caption>
                        <thead>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Category</th>
                            <th>Language</th>
                            
                        </thead>
                
                        @foreach ($books as $book)
                            <tr>
                                <td>{{ $book->title }}</td>
                                <td>{{ $book->authors[0]->author_name }}</td>
                                <td>{{ $book->category->category_name }}</td>
                                <td>{{ $book->language->language_name }}</td>
                            </tr>
                        @endforeach
                
                    </table>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            {{ $books->links() }}
                        </div>
                    </div>

            </div>

        </div>
    </section>


    

@endsection

@section('javascript')
	@parent
@endsection