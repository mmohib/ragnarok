<header> <!-- header started -->
    <nav class="navbar navbar-default"> 

        <div class="">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed pull-right" id="sidebar-reveal" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            
            <span>
                <p class="navbar-brand pull-left">{{ Auth::user()->name }}</p>
                @if (Auth::user()->image)
                    <img class="img-circle user-img" src="/images/profile/{{ Auth::user()->image }}" alt="book cover" />
                @else
                    <img class="img-circle user-img" src="/images/dashboard/user.png" alt="book cover" />
                @endif
            </button>


            <a class="navbar-brand pull-left" href="/">
                <img src="/images/logo.svg" alt="muthoboi-logo" class="logo">
            </a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        
        <div class="right-sidebar clearfix">

        <ul class="nav navbar-nav navbar-left">

            {{-- <li><a href="/create-sell-post" class="btn btn-default navbar-btn">Sell/Exchange Books</a></li>                 --}}

            <li>
                <a href="{{ route('logout') }}"
                    class="btn btn-back navbar-btn"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    Sign out
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>    
            
                                

        </ul>

        </div><!-- /.navbar-collapse -->

        </div><!-- /.container-fluid -->

    </nav>  
</header> <!-- header ended -->