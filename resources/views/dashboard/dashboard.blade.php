@extends('dashboard.layouts.master')

@section('title', 'Welcome')

@section('stylesheet')
	@parent
@endsection

@section('content')
<div id="">

        <section class="dashboard-main-content"> <!-- Main content started -->

            <div class="container"> <!-- container-start -->

                <div class="row hidden-sm hidden-md hidden-lg"> <!-- row-start for device < 768px -->

                    <div class="col-xs-offset-1 col-xs-10">

                        <div class="card" id="download-card">
                                
                            
                            <h2>Total Books</h2>
                            <h1>{{ $totalBooks }}</h1>
                            
                
                        </div>

                        <div class="graph" id="download-graph">
                            {{--  <img src="src/img/download-graph.png" alt="download-graph" class="img-responsive">  --}}
                        </div>
                
                    </div>

                    <div class="col-xs-offset-1 col-xs-10">
                        <div class="card" id="book-card">
                                    
                            <h2>Total Books</h2>
                            <h1>{{ $totalBooks }}</h1>
                                
                        </div>
        
                        <div class="graph" id="book-graph">
                            {{--  <img src="src/img/download-graph.png" alt="download-graph" class="img-responsive">  --}}
                        </div>
                    </div>

                    <div class="col-xs-offset-1 col-xs-10">
                            <div class="card" id="sale-card">
                                    
                                <h2>Total Books on Sell</h2>
                                <h1>{{ $totalSellposts }}</h1>
                                    
                            </div>
        
                            <div class="graph" id="sale-graph">
                                {{--  <img src="src/img/download-graph.png" alt="download-graph" class="img-responsive">  --}}
                            </div>
        
                    </div>

                </div> <!-- row-end for device < 768px -->

                <div class="row hidden-xs"> <!-- row start for device > 768px -->
                    <div class="col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10">
                    
                        <div class="card download-card-selected" id="download-card">
                            
                            <a href="#" id="download-onclick--js">
                                <h2>Total Books</h2>
                                <h1>{{ $totalBooks }}</h1>
                            </a>

                        </div>
        
                        <div class="card book-card-selected" id="book-card">
                            
                            <a href="#" id="book-onclick--js">
                                <h2>Total Books on Sell</h2>
                                <h1>{{ $totalSellposts }}</h1>
                            </a>
                        
                        </div>
        
                        <div class="card sale-card-selected" id="sale-card">
                            
                            <a href="#" id="sale-onclick--js">
                                <h2>Total Books on Exchange</h2>
                                <h1>{{ $totalExchangeposts }}</h1>
                            </a>
                            
                        </div>
        
                    </div>
        
                </div> <!-- row end for device > 768px -->


                <div class="row hidden-xs graph-row"> <!-- graph image for device > 768px start-->
                    <div class="col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10">
                        <div class="graph">
                            {{--  <img id="graph-onclick--js" src="src/img/download-graph.png" alt="download-graph" class="img-responsive">  --}}
                        </div>
                    </div>
                </div> <!-- graph image for device > 768px end-->


            </div> <!-- container-end -->


        </section> <!-- Left Sidebar ended -->


    </div>
@endsection

@section('javascript')
	@parent
@endsection