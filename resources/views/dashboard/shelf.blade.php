@extends('dashboard.layouts.master')

@section('title', 'Shelf')

@section('stylesheet')
	@parent
@endsection

@section('content')


    <section class="report-main-content-desktop hidden-xs">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <form action="{{ route('shelf-filter') }}" method="get" class="form-inline desktop-form">

                        <h4>Filter your books by Status</h4>
                        <div id="desktop-select-1" class="form-group form-element-desktop">
                            <label for=""></label>
                            <select name="status" class="form-control">
                                <option disabled selected>Select Status</option>
                                <option value="1">Book is on the Shelf</option>
                                <option value="2">Book is on the Sell</option>
                                <option value="3">Book is on the Exchange</option>
                                <option value="4">Lend to Borrower</option>
                            </select>
                        </div>

                        <button type="submit" id="dekstop-data-btn" class="btn btn-default btn-success btn-lg">Submit</button>

                    </form>
                </div>
            </div>

            <div class="table-responsive report-table-desktop">
                    <table class="table table-bordered">
                        <caption><h3>All Books</h3></caption>
                        <thead>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Category</th>
                            <th>Language</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            
                        </thead>
                
                        @foreach ($books as $book)
                            <tr>
                                
                                <td><a href="{{ route('dashboard-book', ['id' => $book->id])}}">{{ $book->title }}</a></td>
                                <td>{{ $book->authors[0]->author_name }}</td>
                                <td>{{ $book->category->category_name }}</td>
                                <td>{{ $book->language->language_name }}</td>
                                @if ($book->users[0]->pivot->book_sell_status)
                                    <td>Book is on the Sell</td>
                                @elseif ($book->users[0]->pivot->book_exchange_status)
                                    <td>Book is on the Exchange</td>
                                @elseif ($book->users[0]->pivot->book_lending_status)
                                    <td>Lend to Borrower</td>
                                @else
                                    <td>Book is on the Shelf</td>
                                @endif
                                <td><a href="{{ route('edit-book', ['id' => $book->id])}}">Edit</a></td>
                                <td><a href="{{ route('delete-book', ['id' => $book->id])}}">Delete</a></td>
                                
                            </tr>
                        @endforeach
                
                    </table>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            {{ $books->links() }}
                        </div>
                    </div>

            </div>

        </div>
    </section>


    

@endsection

@section('javascript')
	@parent
@endsection