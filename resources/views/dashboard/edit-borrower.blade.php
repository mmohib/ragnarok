@extends('dashboard.layouts.master') 

@section('title', 'Welcome') 

@section('stylesheet') 
    @parent 
@endsection 

@section('content')
<div class="container" id="create-book">

    <div class="row">
        <h1 class="text-center">Update Borrower</h1>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('update-borrower', ['id' => $borrower->id] )}}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="book-name">Name</label>
                <input id="book-name" type="text" class="form-control" name="name" value="{{ $borrower->borrower_name }}">
                </div>

                <div class="form-group">
                    <label for="author">Email</label>
                    <input id="book-author" type="text" class="form-control" name="email" value="{{ $borrower->email }}">
                </div>

                <div class="form-group">
                    <label for="author">Mobile</label>
                    <input id="book-author" type="text" class="form-control" name="mobile" value="{{ $borrower->phone_number }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection 
