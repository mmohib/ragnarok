@extends('dashboard.layouts.master')

@section('title', 'All User')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section class="report-main-content-desktop hidden-xs">
        <div class="container">
            <div class="table-responsive report-table-desktop">
                    <table class="table table-bordered">
                        <caption><h3>All Users ( {{$counts}} )</h3></caption>
                        <thead>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Join Date</th>
                            
                        </thead>
                
                        @foreach ($users as $user)
                            <tr>
                                <td><a href="{{ route('profile', ['id' => $user->id ])}}">{{ $user->name }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->mobile }}</td>
                                <td>{{ $user->created_at->format('d-m-Y') }}</td>
                            </tr>
                        @endforeach
                
                    </table>

            </div>

        </div>
    </section>
@endsection

@section('javascript')
	@parent
@endsection