@extends('dashboard.layouts.master') 

@section('title', 'Welcome') 

@section('stylesheet') 
    @parent 
@endsection 

@section('content')
<div class="container" id="create-book">

    <div class="row">
        <h1 class="text-center">Create Book</h1>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('update-book', ['id' => $book[0]->id]) }}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="book-name">Book Name</label>
                <input id="book-name" type="text" class="form-control" name="title" value="{{ $book[0]->title }}">
                </div>

                <div class="form-group">
                    <label for="author">Author</label>
                    <input id="book-author" type="text" class="form-control" name="author" value="{{ $book[0]->authors[0]->author_name }}">
                </div>

                <div class="form-group">
                    <label for="category">Category</label>
                    <input id="book-category" type="text" class="form-control" name="category" value="{{ $book[0]->category->category_name }}">
                </div>

                <div class="form-group">
                    <label for="language">Language</label>
                    <input id="book-language" type="text" class="form-control" name="language" value="{{ $book[0]->language->language_name }}">
                </div>

                <div class="form-group">
                    <label for="category">Publisher</label>
                    @if (!$book[0]->users[0]->pivot->publisher)
                    <input id="book-language" type="text" class="form-control" name="publisher" placeholder="Publisher Name">

                    @else
                    <input id="book-language" type="text" class="form-control" name="publisher" value="{{ $book[0]->users[0]->pivot->publisher }}">
                    @endif
                </div>

                <div class="form-group">
                    <label for="language">Edition</label>

                    @if (!$book[0]->users[0]->pivot->edition)
                    <input id="book-language" type="text" class="form-control" name="edition" placeholder="Edition">

                    @else
                    <input id="book-language" type="text" class="form-control" name="edition" value="{{ $book[0]->users[0]->pivot->edition }}">
                    @endif
                </div>

                <div class="form-group">
                    <label for="book-cover">Upload Book Cover</label>
                    <input type="file" id="book-cover" name="book_cover">
                    <p class="help-block">Upload a parfect book cover.</p>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection 
