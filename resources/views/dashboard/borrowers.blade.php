@extends('dashboard.layouts.master')

@section('title', 'All Borrowers')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="dashboard-book">
        <div class="container">

            <div class="row">
                <div class="col-md-offset-1 col-md-11">
                    <table class="table table-bordered borrower-table">
                        <caption><h3>Borrower List</h3></caption>
                        <thead>
                            <th>Name</th>
                            <th>Phone No</th>
                            <th>Email</th>
                            <th>Edit</th>
                        </thead>
                
                        
                        @foreach ($borrowers as $borrower)
                            <tr>
                                <td><a href="{{ route('borrower', ['id' => $borrower->id])}}">{{ $borrower->borrower_name }}</a></td>
                                <td>{{ $borrower->email }}</td>
                                <td>{{ $borrower->phone_number }}</td>
                                <td><a href="{{ route('edit-borrower', ['id' => $borrower->id ] ) }}">Edit</a></td>
                            </tr>
                        @endforeach
                        
                
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('javascript')
	@parent
@endsection