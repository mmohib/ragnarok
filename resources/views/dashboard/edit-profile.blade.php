@extends('dashboard.layouts.master') 

@section('title', 'Welcome') 

@section('stylesheet') 
    @parent 
@endsection 

@section('content')
<div class="container" id="create-book">

    <div class="row">
        <h1 class="text-center">Update Profile</h1>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('update-profile', ['id' => $user->id] )}}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="book-name">Name</label>
                <input id="book-name" type="text" class="form-control" name="name" value="{{ $user->name }}">
                </div>

                <div class="form-group">
                    <label for="author">Mobile</label>
                    <input id="book-author" type="text" class="form-control" name="mobile" value="{{ $user->mobile }}">
                </div>

                <div class="form-group">
                    <label for="author">Address</label>
                    <textarea name="address" class="form-control" id="" cols="30" rows="5">{{ $user->address }}</textarea>
                </div>

                <div class="form-group">
                    <label for="book-cover">Upload Profile picture</label>
                    <input type="file" id="book-cover" name="profile_pic">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection 
