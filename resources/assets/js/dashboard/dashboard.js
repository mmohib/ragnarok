/*
    Dashboard 
    js 
    start
*/

$(document).ready(function () {
    $('.download-card-selected').addClass('download-card-active');
});


$('#sidebar-reveal').click(function () {

    $('.right-sidebar').toggleClass('reveal-nav');
});

$('.arrow-bottom').click(function () {
    $('.fixed-left-sidebar').toggleClass('fixed-left-extended-sidebar');
    $('.arrow-bottom').addClass('arrow-bottom-extended');

    let icon = $('.arrow-bottom i');

    if (icon.hasClass('ion-ios-arrow-forward')) {
        icon.addClass('ion-ios-arrow-back');
        icon.removeClass('ion-ios-arrow-forward');
    }
    else {
        icon.addClass('ion-ios-arrow-forward');
        icon.removeClass('ion-ios-arrow-back');
    }
});

var graph = $('#graph-onclick--js'); // graph image id that will be changed on click
var downloadCard = $('.download-card-selected'); // used for active or inactive card 
var bookCard = $('.book-card-selected'); // used for active or inactive card 
var saleCard = $('.sale-card-selected'); // used for active or inactive card 

$('#download-onclick--js').click(function () {
    graph.attr('src', 'src/img/download-graph.png');
    downloadCard.addClass('download-card-active');
    bookCard.removeClass('book-card-active');
    saleCard.removeClass('sale-card-active');
});


$('#book-onclick--js').click(function () {
    graph.attr('src', 'src/img/book-graph.png');
    bookCard.addClass('book-card-active');
    downloadCard.removeClass('download-card-active');
    saleCard.removeClass('sale-card-active');

});

$('#sale-onclick--js').click(function () {
    graph.attr('src', 'src/img/sale-graph.png');
    saleCard.addClass('sale-card-active');
    bookCard.removeClass('book-card-active');
    downloadCard.removeClass('download-card-active');
});


/*
    Dashboard 
    js
    end
*/




/*
    log-in page
    js
    end
*/



/*------------------------------------------------------------------------*/



/*
    report page
    js
    start
*/


$(document).ready(function () {
    var date_input = $("input.date"); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    var options = {
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
        orientation: 'top right',
        allowInputToggle: true
    };
    date_input.datepicker(options);
})



/*
    report page
    js
    end
*/

const bookOption = $('.switch label');

bookOption.click(function() {
    bookOption.removeClass('active');
    $(this).addClass('active');
})




