<?php

use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Authentication
Auth::routes();

// Facebook sociolite
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/search', 'WelcomeController@search')->name('search');

Route::get('/book/{book}/{user}', 'BookController@show')->name('book');
Route::get('/book/{id}', 'BookController@showWithoutUser')->name('book-non-user');
Route::get('/book-name', 'BookController@bookName')->name('book-name');

Route::get('/all-sells', 'SellPostController@index')->name('all-sells');
Route::get('/all-exchanges', 'ExchangePostController@index')->name('all-exchanges');


Route::group(['middleware' => 'auth', 'prefix' => 'dashboard'], function () {

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/users', 'UserController@index')->name('admin-users');
    Route::get('/user/{id}', 'UserController@show')->name('profile');
    Route::get('/user/edit/{id}', 'UserController@edit')->name('edit-profile');
    Route::post('/user/update/{id}', 'UserController@update')->name('update-profile');

    Route::get('/all-books', 'BookController@adminBooks')->name('admin-books');
    Route::get('/create-book', 'BookController@create')->name('create-book');
    Route::post('/create-book', 'BookController@store')->name('store-book');
    Route::get('/shelf', 'BookController@shelf')->name('shelf');
    Route::get('/shelf/filter', 'BookController@filterShelf')->name('shelf-filter');
    Route::get('/book/{id}', 'BookController@dashboardShow')->name('dashboard-book');
    Route::get('/book/edit/{id}', 'BookController@dashboardEdit')->name('edit-book');
    Route::post('/book/update/{id}', 'BookController@dashboardUpdate')->name('update-book');
    Route::get('/book/delete/{id}', 'BookController@dashboardDelete')->name('delete-book');
    Route::post('/change-option', 'BookController@changeOption')->name('change-option');
    Route::post('/extra-info', 'BookController@updateExtraInfo')->name('extra-info');

    Route::get('/sell-post-option', 'SellPostController@sellOption')->name('sell-post-option');
    Route::get('/create-sell/{id}', 'SellPostController@create')->name('create-sell');
    Route::post('/create-sell', 'SellPostController@store')->name('store-sell');

    Route::get('/exchange-post-option', 'ExchangePostController@exchangeOption')->name('exchange-post-option');    
    Route::get('/create-exchange/{id}', 'ExchangePostController@create')->name('create-exchange');
    Route::post('/create-exchange', 'ExchangePostController@store')->name('store-exchange');


    Route::get('/borrowers', 'BorrowerController@index')->name('borrowers');     
    Route::get('/borrower-name', 'BorrowerController@borrowerName')->name('borrower-name');     
    Route::get('/borrower/{id}', 'BorrowerController@show')->name('borrower');   
    Route::get('/borrower/edit/{id}', 'BorrowerController@edit')->name('edit-borrower'); 
    Route::post('/borrower/update/{id}', 'BorrowerController@update')->name('update-borrower'); 
    Route::get('/create-borrower/{id}', 'BorrowerController@create')->name('create-borrower');
    Route::post('/create-borrower', 'BorrowerController@store')->name('store-borrower');

    Route::post('/create-review', 'ReviewController@store')->name('store-review');

    Route::get('/category-name', 'CategoryController@categoryName')->name('category-name');
    Route::get('/language-name', 'LanguageController@languageName')->name('language-name');
    Route::get('/author-name', 'AuthorController@authorName')->name('author-name');
});