<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\SellPost;
use App\ExchangePost;
use App\Review;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellPosts = SellPost::where('sell_status', false)->take(4)->latest()->get();
        $exchangePosts = ExchangePost::where('exchange_status', false)->take(4)->latest()->get();
        $reviews = Review::latest()->take(4)->latest()->get();
        return view('welcome', compact('sellPosts', 'exchangePosts', 'reviews'));
    }

    public function search(Request $request)
    {
        $searchTerm = $request->input('q');
        $books = Book::where('title','LIKE','%'.$searchTerm.'%')->get();
        return view('search-results', compact('books'));
    }
}
