<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin')->only('index');
    }

    public function index()
    {
        $users = User::all();
        $counts = User::all()->count();
        return view('dashboard.admin-users', compact('users', 'counts'));
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('dashboard.profile', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.edit-profile', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'mobile' => 'required|string'
        ]);

        if($request->has('profile_pic')) {
            $image = $request->file('profile_pic');
			$fileName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/profile'), $fileName);
        } else {
            $fileName = NULL;
        }

        User::find($id)->update([
            'name' => $request->name,
            'mobile' => $request->mobile,
            'address' => $request->address,
            'image' => $fileName
        ]);

        $request->session()->flash('status', 'Profile Updated successfully!');
        return redirect()->route('profile');
    }
}
