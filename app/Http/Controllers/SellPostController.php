<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SellPost;
use App\User;


class SellPostController extends Controller
{
    public function index()
    {
        $sellPosts = SellPost::where('sell_status', false)->paginate(16);
        return view('sell-posts', compact('sellPosts'));
    }

    public function sellOption()
    {
        return view('dashboard.sell-post-option');
    }

    public function create(Request $request, $id)
    {
        return view('dashboard.create-sell', compact('id'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'price' => 'required',
            'book_id' => 'required'
        ]);

        $userId = $request->user()->id;
        $bookId = $request->input('book_id');

        $sellPost = SellPost::create([
            'price' => $request->input('price'),
            'book_id' => $bookId,
            'user_id' => $userId
        ]);

        User::where('id', $userId)
            ->update([
            'mobile' => $request->input('mobile'),
            'address' => $request->input('address')            
        ]);

        User::find($userId)->books()->updateExistingPivot($bookId, [
            'book_sell_status' => true
        ]);

        $request->session()->flash('status', 'Sell Post Created successfully!');

        return redirect()->route('shelf');

    }
}
