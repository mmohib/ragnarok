<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SellPost;
use App\User;
use App\Book;
use App\ExchangePost;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalBooks = Book::with('users')->whereHas('users', function($q) {
                       $q->where('user_id', Auth::id());
            })->count();
        $totalSellposts = SellPost::where('user_id', Auth::id())->count();
        $totalExchangeposts = ExchangePost::where('user_id', Auth::id())->count();
        return view('dashboard.dashboard', compact('totalBooks', 'totalSellposts', 'totalExchangeposts'));
    }
}
