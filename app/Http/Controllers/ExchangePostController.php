<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExchangePost;
use App\User;

class ExchangePostController extends Controller
{
    public function index()
    {
        $exchangePosts = ExchangePost::where('exchange_status', false)->paginate(16);
        return view('exchange-posts', compact('exchangePosts'));
    }

    public function exchangeOption()
    {
        return view('dashboard.exchange-post-option');
    }

    public function create(Request $request, $id)
    {
        return view('dashboard.create-exchange', compact('id'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'book_id' => 'required',
            'note' => 'required'
        ]);

        $userId = $request->user()->id;
        $bookId = $request->input('book_id');

        $exchangePost = ExchangePost::create([
            'book_id' => $bookId,
            'user_id' => $userId,
            'exchange_post' => $request->input('note')
        ]);

        User::where('id', $userId)
            ->update([
            'mobile' => $request->input('mobile'),
            'address' => $request->input('address')            
        ]);

        User::find($userId)->books()->updateExistingPivot($bookId, [
            'book_exchange_status' => true
        ]);

        $request->session()->flash('status', 'Exchange Post Created successfully!');

        return redirect()->route('shelf');
    }
}
