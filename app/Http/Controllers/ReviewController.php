<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Rating;

class ReviewController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'rating' => 'required',
            'review' => 'required',
            'book_id' => 'required'
        ]);

        $bookId = $request->input('book_id');
        $userId = $request->user()->id;

        Review::create([
            'review' => $request->input('review'),
            'user_id' => $userId,
            'book_id' => $bookId
        ]);

        Rating::create([
            'value' => $request->input('rating'),
            'user_id' => $userId,
            'book_id' => $bookId
        ]);

        $request->session()->flash('status', 'Rating & Review added successfully!');
        return redirect()->route('dashboard-book', ['id' => $bookId]);
    }
}
