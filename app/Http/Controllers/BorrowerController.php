<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Borrower;
use App\User;
use Auth;
use Carbon\Carbon;

class BorrowerController extends Controller
{
    public function index()
    {
        $borrowers = Borrower::where('user_id', Auth::id())->get();
        return view('dashboard.borrowers', compact('borrowers'));
    }
    
    public function show(Request $request, $id)
    {
        $borrower = Borrower::find($id);
        $books = $borrower->books;
        return view('dashboard.borrower', compact('borrower', 'books'));
    }

    public function create(Request $request, $id)
    {
        return view('dashboard.create-borrower', compact('id'));
    }

    public function edit($id)
    {
        $borrower = Borrower::find($id);
        return view('dashboard.edit-borrower', compact('borrower'));
    }

    public function borrowerName(Request $request)
    {
        return Borrower::where('user_id', $request->user()->id)->get();
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string',
            'mobile' => 'required|string',
            'email' => 'required|string',
            'lend_date' => 'required|date|before_or_equal:today',
            'return_date' => 'required|date|after_or_equal:lend_date', //Only accept if return date > lend date
            'book_id' => 'required',
        ]);

        //Get user ID
        $user_id = $request->user()->id;
        $bookid = $request->input('book_id');

        //Find the borrower instance
        $email = $request->input('email');
        $borrower = Borrower::where([['email', $email], ['user_id', $user_id]])->get();

        // Create Borrower if not exist
        if ($borrower->isEmpty()) {
            $borrower = new Borrower;
            $borrower->borrower_name = $request->input('name');
            $borrower->phone_number = $request->input('mobile');
            $borrower->email = $request->input('email');
            $borrower->user_id = $user_id;
            $borrower->save();

            $borrower = Borrower::where('id', $borrower->id)->get();
        }


        //Format the date according to DB
        $lend_date = $request->input('lend_date');
		$fomatted_lend_date = Carbon::parse($lend_date)->format('Y-m-d');

        $return_date = $request->input('return_date');
		$fomatted_return_date = Carbon::parse($return_date)->format('Y-m-d');

        // Add book to pivot table and update book status
        $borrower[0]->books()->attach($bookid, [
            'lend_date' => $fomatted_lend_date,
            'return_date' => $fomatted_return_date
        ]);

        // for updating pivot table according to the user and book id
        User::find($user_id)->books()->updateExistingPivot($bookid, [
            'book_lending_status' => true
        ]);
        
        //Return a flash message
        $request->session()->flash('status', 'Add books to borrower Successfully!');
        return redirect()->route('shelf');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'mobile' => 'required|string',
            'email' => 'required|string'
        ]);

        Borrower::find($id)->update([
            'borrower_name' => $request->name,
            'email' =>$request->email,
            'phone_number' => $request->mobile
        ]);

        $request->session()->flash('status', 'Update borrower Successfully!');
        return redirect()->route('borrower', ['id' => $id]);
    }

}
