<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role_name)
    {
        if(! Auth::check())
        {
            return redirect('/');
        }
        else
        {
            if ( ! Auth::user()->hasRole($role_name))
            {
                return redirect('/');
            }
        }

      return $next($request);

    //   return $next($request);
    }
}
