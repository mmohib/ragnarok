<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellImage extends Model
{
    protected $fillable = [
        'sell_post_id', 'sell_image_1',
        'sell_image_2', 'sell_image_3'
    ];

    public function sellPost() {
        return $this->belongsTo(SellPost::class);
    }
}
