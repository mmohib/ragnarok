<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangePost extends Model
{
    protected $fillable = [
        'book_id', 'user_id', 'exchange_post'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function book() {
        return $this->belongsTo(Book::class);
    }

    public function exchangeImages() {
        return $this->hasMany(ExchangeImage::class);
    }
}
