<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Book extends Model
{
    protected $fillable = [
        'title', 'category_id', 'language_id',
        'image', 'purchased_date', 'published_date',
        'edition', 'publisher', 'book_sell_status', 'book_exchange_status',
        'book_exchanged', 'book_sold'
    ];

    public function users() {
        return $this->belongsToMany(User::class)
            ->withPivot('book_public_status', 'book_lending_status', 'book_sell_status',
                'book_exchange_status',  'price', 'purchased_date', 'image', 'book_sold',
                'book_exchanged', 'edition', 'publisher')
            ->wherePivot('user_id', Auth::id());
    }

    public function allUser() {
        return $this->belongsToMany(User::class)
            ->withPivot('book_public_status', 'book_lending_status', 'book_sell_status',
                'book_exchange_status',  'price', 'purchased_date', 'image', 'book_sold', 'book_exchanged');
    }

    public function authors() {
        return $this->belongsToMany(Author::Class);
    }

    public function borrowers() {
        return $this->belongsToMany(Borrower::class)
        ->withPivot('borrower_id', 'book_id', 'lend_date', 'return_date',
            'orginal_return_date')
        ->where('user_id', Auth::user()->id);
    }
    

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function language() {
        return $this->belongsTo(Language::class);
    }

    public function exchangePosts() {
        return $this->hasMany(ExchangePost::class);
    }

    public function sellPosts() {
        return $this->hasMany(SellPost::class);
    }

    public function reviews() {
        return $this->hasMany(Review::class);
    }

    public function ratings() {
        return $this->hasMany(Rating::class);
    }

    public function ratingsWithUser() {
        return $this->hasMany(Rating::class)->where('user_id', Auth::id());
    }

    public function reviewsWithUser() {
        return $this->hasMany(Review::class)->where('user_id', Auth::id());
    }

    public function scopeFilter($query, $filter)
    {
        if ($status = $filter['status'])
        {
            //Return books that are on the shelf
            if ($status == 1) {
                $pivot = $this->users()->getTable();

                $query->whereHas('users', function ($q) use ($status, $pivot) {
                    $q->where([
                        ["{$pivot}.book_sell_status", 0],
                        ["{$pivot}.book_exchange_status", 0],
                        ["{$pivot}.book_lending_status", 0],
                    ]);
                });
            }

            //Return books that are on the sell
            if ($status == 2) {
                $pivot = $this->users()->getTable();

                $query->whereHas('users', function ($q) use ($status, $pivot) {
                    $q->where("{$pivot}.book_sell_status", 1);
                });
            }

            //Return books that are on the exchange
            if ($status == 3) {
                $pivot = $this->users()->getTable();

                $query->whereHas('users', function ($q) use ($status, $pivot) {
                    $q->where("{$pivot}.book_exchange_status", 1);
                });
            }

            //Return books that are on the lend
            if ($status == 4) {
                $pivot = $this->users()->getTable();

                $query->whereHas('users', function ($q) use ($status, $pivot) {
                    $q->where("{$pivot}.book_lending_status", 1);
                });
            }
        }
    }
}
