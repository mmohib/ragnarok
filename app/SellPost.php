<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellPost extends Model
{
    protected $fillable = [
        'book_id', 'user_id', 'price',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function book() {
        return $this->belongsTo(Book::class);
    }

    public function sellImages() {
        return $this->hasMany(SellImage::class);
    }
}
