<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sell_post_id')->unsigned();
            $table->foreign('sell_post_id')->references('id')->on('sell_posts');
            $table->string('sell_image_1')->nullable();
            $table->string('sell_image_2')->nullable();
            $table->string('sell_image_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_images');
    }
}
