# Bookshelf

## Installation

- git clone <project-clone-link>
- cd ragnarok
- composer install
- npm install
- cp .env.example .env
- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan serve


[Laravel documentation](https://laravel.com/docs)
## About Projects

This is a university Projects.

## Contributor

- [Maruf](https://github.com/marufMunshi)
- [Mohib](https://github.com/mbmohib)


## Todos
* User profile
* Check borrower book added to correct user
* All review books page
* In Search add books that are not in sell/exchage
* Add totol borrower books count in dashboard

## Notes

On Linux (or Mac) we have a max number of system watchers we can place at an IO level (from my understanding). So for large projects. Sometimes it shows this 
Error: watch elm-stuff ENOSPC

Solution: 
- echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).